﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
	public partial class Form1 : Form
	{

		public Copo meuCopo;

		
		public Form1()
		{
			InitializeComponent();
			meuCopo = new Copo();

			vinho.Visible = false;
			cerveja.Visible = false;
			cafe.Visible = false;

		}

		
		private void buttonServir_Click(object sender, EventArgs e)
		{


			#region Erro Capacidade e ShowImage

			if (comboBoxBebidas.Text == "Café")
			{
				if (trackBarQuantidade.Value > 30)
				{
					vinho.Visible = false;
					cerveja.Visible = false;
					cafe.Visible = false;

					label4.Text = "Err.";
					label5.Text = "Err.";
				}
				else
				{
					cafe.Visible = true;
					vinho.Visible = false;
					cerveja.Visible = false;
					

					label4.Text = meuCopo.Capacidade.ToString() + "%";
					label5.Text = meuCopo.Liquido.ToString();
				}
			}
			if (comboBoxBebidas.Text == "Cerveja")
			{
				if (trackBarQuantidade.Value > 90)
				{
					vinho.Visible = false;
					cerveja.Visible = false;
					cafe.Visible = false;

					label4.Text = "Err.";
					label5.Text = "Err.";
				}
				else
				{
					cerveja.Visible = true;
					vinho.Visible = false;					
					cafe.Visible = false;

					label4.Text = meuCopo.Capacidade.ToString() + "%";
					label5.Text = meuCopo.Liquido.ToString();
				}
			}
			if (comboBoxBebidas.Text == "Vinho")
			{
				if (trackBarQuantidade.Value > 60)
				{
					vinho.Visible = false;
					cerveja.Visible = false;
					cafe.Visible = false;

					label4.Text = "Err.";
					label5.Text = "Err.";
				}
				else
				{
					vinho.Visible = true;
					cerveja.Visible = false;
					cafe.Visible = false;

					label4.Text = meuCopo.Capacidade.ToString() + "%";
					label5.Text = meuCopo.Liquido.ToString();
				}
			}
			#endregion

			//label4.Text = meuCopo.Capacidade.ToString() + "%";
			//label5.Text = meuCopo.Liquido.ToString();

		}

		private void OnFrameChanged(object sender, EventArgs e)
		{
			// frame change
			
		}

		
		private void trackBarQuantidade_Scroll(object sender, EventArgs e)
		{

			labelQuantidade.Text = trackBarQuantidade.Value.ToString() + "%";

			meuCopo.Capacidade = trackBarQuantidade.Value;

			#region Aviso Quantidade

			if (comboBoxBebidas.Text == "Café")
			{
				if (trackBarQuantidade.Value > 30)
				{
					avisoQuatidade.Text = "Cuidado, o copo não suporta essa quantidade.";
				}
				else
				{
					avisoQuatidade.Text = null;
				}
			}

			if (comboBoxBebidas.Text == "Cerveja")
			{
				if (trackBarQuantidade.Value > 90)
				{
					avisoQuatidade.Text = "Cuidado, o copo não suporta essa quantidade.";
				}
				else
				{
					avisoQuatidade.Text = null;
				}
			}

			if (comboBoxBebidas.Text == "Vinho")
			{
				if (trackBarQuantidade.Value > 60)
				{
					avisoQuatidade.Text = "Cuidado, o copo não suporta essa quantidade.";
				}
				else
				{
					avisoQuatidade.Text = null;
				}
			}

			#endregion




		}
		
		private void comboBoxBebidas_SelectedIndexChanged(object sender, EventArgs e)
		{

			meuCopo.Liquido = comboBoxBebidas.SelectedItem.ToString();

			
		}

		
	}
}
