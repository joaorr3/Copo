﻿namespace Copo
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.buttonServir = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxBebidas = new System.Windows.Forms.ComboBox();
			this.trackBarQuantidade = new System.Windows.Forms.TrackBar();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.labelQuantidade = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label5 = new System.Windows.Forms.Label();
			this.avisoQuatidade = new System.Windows.Forms.Label();
			this.cafe = new System.Windows.Forms.PictureBox();
			this.cerveja = new System.Windows.Forms.PictureBox();
			this.vinho = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.trackBarQuantidade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cafe)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cerveja)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vinho)).BeginInit();
			this.SuspendLayout();
			// 
			// buttonServir
			// 
			this.buttonServir.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonServir.Location = new System.Drawing.Point(409, 290);
			this.buttonServir.Name = "buttonServir";
			this.buttonServir.Size = new System.Drawing.Size(198, 32);
			this.buttonServir.TabIndex = 1;
			this.buttonServir.Text = "Servir";
			this.buttonServir.UseVisualStyleBackColor = true;
			this.buttonServir.Click += new System.EventHandler(this.buttonServir_Click);
			// 
			// label1
			// 
			this.label1.Enabled = false;
			this.label1.Location = new System.Drawing.Point(292, 439);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 0;
			// 
			// comboBoxBebidas
			// 
			this.comboBoxBebidas.FormattingEnabled = true;
			this.comboBoxBebidas.Items.AddRange(new object[] {
            "Cerveja",
            "Café",
            "Vinho"});
			this.comboBoxBebidas.Location = new System.Drawing.Point(409, 132);
			this.comboBoxBebidas.Name = "comboBoxBebidas";
			this.comboBoxBebidas.Size = new System.Drawing.Size(198, 21);
			this.comboBoxBebidas.TabIndex = 2;
			this.comboBoxBebidas.SelectedIndexChanged += new System.EventHandler(this.comboBoxBebidas_SelectedIndexChanged);
			// 
			// trackBarQuantidade
			// 
			this.trackBarQuantidade.Location = new System.Drawing.Point(409, 212);
			this.trackBarQuantidade.Maximum = 100;
			this.trackBarQuantidade.Name = "trackBarQuantidade";
			this.trackBarQuantidade.Size = new System.Drawing.Size(140, 45);
			this.trackBarQuantidade.TabIndex = 3;
			this.trackBarQuantidade.Scroll += new System.EventHandler(this.trackBarQuantidade_Scroll);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(405, 106);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(162, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "Escolha a bebida";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(405, 186);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(202, 23);
			this.label3.TabIndex = 5;
			this.label3.Text = "Escolha a quantidade";
			// 
			// labelQuantidade
			// 
			this.labelQuantidade.AutoSize = true;
			this.labelQuantidade.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelQuantidade.Location = new System.Drawing.Point(548, 212);
			this.labelQuantidade.Name = "labelQuantidade";
			this.labelQuantidade.Size = new System.Drawing.Size(37, 23);
			this.labelQuantidade.TabIndex = 6;
			this.labelQuantidade.Text = "0%";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial Black", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(201, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(0, 52);
			this.label4.TabIndex = 7;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(59, 33);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(126, 306);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial Black", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(201, 178);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(0, 52);
			this.label5.TabIndex = 9;
			// 
			// avisoQuatidade
			// 
			this.avisoQuatidade.AutoSize = true;
			this.avisoQuatidade.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.avisoQuatidade.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.avisoQuatidade.Location = new System.Drawing.Point(409, 243);
			this.avisoQuatidade.Name = "avisoQuatidade";
			this.avisoQuatidade.Size = new System.Drawing.Size(0, 15);
			this.avisoQuatidade.TabIndex = 10;
			// 
			// cafe
			// 
			this.cafe.Enabled = false;
			this.cafe.Image = ((System.Drawing.Image)(resources.GetObject("cafe.Image")));
			this.cafe.Location = new System.Drawing.Point(59, 33);
			this.cafe.Name = "cafe";
			this.cafe.Size = new System.Drawing.Size(126, 306);
			this.cafe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.cafe.TabIndex = 11;
			this.cafe.TabStop = false;
			// 
			// cerveja
			// 
			this.cerveja.Enabled = false;
			this.cerveja.Image = ((System.Drawing.Image)(resources.GetObject("cerveja.Image")));
			this.cerveja.Location = new System.Drawing.Point(59, 33);
			this.cerveja.Name = "cerveja";
			this.cerveja.Size = new System.Drawing.Size(126, 306);
			this.cerveja.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.cerveja.TabIndex = 12;
			this.cerveja.TabStop = false;
			// 
			// vinho
			// 
			this.vinho.Enabled = false;
			this.vinho.Image = ((System.Drawing.Image)(resources.GetObject("vinho.Image")));
			this.vinho.Location = new System.Drawing.Point(59, 33);
			this.vinho.Name = "vinho";
			this.vinho.Size = new System.Drawing.Size(126, 306);
			this.vinho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.vinho.TabIndex = 13;
			this.vinho.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(658, 417);
			this.Controls.Add(this.vinho);
			this.Controls.Add(this.cerveja);
			this.Controls.Add(this.cafe);
			this.Controls.Add(this.avisoQuatidade);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.labelQuantidade);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.trackBarQuantidade);
			this.Controls.Add(this.comboBoxBebidas);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonServir);
			this.Name = "Form1";
			this.ShowIcon = false;
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.trackBarQuantidade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cafe)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cerveja)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vinho)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button buttonServir;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxBebidas;
		private System.Windows.Forms.TrackBar trackBarQuantidade;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelQuantidade;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label avisoQuatidade;
		private System.Windows.Forms.PictureBox cafe;
		private System.Windows.Forms.PictureBox cerveja;
		private System.Windows.Forms.PictureBox vinho;
	}
}

